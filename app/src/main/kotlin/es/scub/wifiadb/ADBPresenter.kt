package es.scub.wifiadb

import android.content.Context
import android.graphics.drawable.Icon
import android.service.quicksettings.Tile
import android.support.annotation.DrawableRes
import es.sqcub.wifiadb.R

internal class ADBPresenter(private val mContext: Context, private val mPresentingUI: ADBPresenter.ADBPresentingUI) : ADBManager.OnAdbStatusChangedListener {

    override fun onADBStatusChanged(status: ADBManager.ADBStatusResult) {
        val description: String
        val state: Int

        if (status.adbStatus == ADBManager.ADB_STATUS_NO_SU) {
            description = mContext.getString(R.string.no_su)
            state = Tile.STATE_UNAVAILABLE
        } else if (status.adbStatus == ADBManager.ADB_STATUS_STARTING) {
            description = mContext.getString(R.string.starting)
            state = Tile.STATE_ACTIVE
        } else if (status.adbStatus == ADBManager.ADB_STATUS_STOPPING) {
            description = mContext.getString(R.string.stopping)
            state = Tile.STATE_INACTIVE
        } else if (status.adbStatus == ADBManager.ADB_STATUS_STARTED) {
            description = String.format("%s:%s", status.adbip, status.adbPort)
            state = Tile.STATE_ACTIVE
        } else {
            description = mContext.getString(R.string.wifi_adb)
            state = Tile.STATE_INACTIVE
        }
        mPresentingUI.onADBUIUpdate(description, state)
    }

    internal interface ADBPresentingUI {
        fun onADBUIUpdate(text: String, int: Int)
    }
}
